import Component from './Component.js';

class Img extends Component {
    constructor(sourceImg) {
        super(`img`, { name: `src`, value: sourceImg });
    }
}

export default Img;